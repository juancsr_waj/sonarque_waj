# SonarQube Whale and Jaguar

## Como ejecutar

1. Crear red **sonarnet**

`docker network create sonarnet`

2. Ejecutar docker-compose 

`docker-compose up`

En docker-compose.yml

      Comandos utiles:
      
       docker-compose up                            # ejecuta dokcer-compose.yml y crea los contenedores
       docker-compose restart sonarqube             # reinica contenedor
       docker-compose down                          # finaliza contenedor
       docker-compose logs -f sonarqube             # obtenemos logs
       docker exec -i -t <container_id> /bin/bash   # entramos por bash al contenedor by ID
       docker exec -i -t <container_id> /bin/bash   # entramos por bash al contenedor by ID
       docker container stop <container_id>         # finaliza contenedor

3. En el navegador ir a `localhost:9000`

## Sonar-scanner

Dentro del proyecto puede existir un fichero *sonar-project.properties* con la siguiente estructura:

```
sonar.projectKey=banrep-chatbot
sonar.projectName=banrep-chatbot
sonar.sourceEncoding=UTF-8
sonar.sources=./
sonar.exclusion=README.md,babelrc,/assets/*,packge.json,*.json,/node_modules/*
```

* **sonar.projectName:** Nombre del proyeto en sonarqube server
* **sonar.sources:**: Ficheros que se tienen en cuenta para escanear
* **sonar.exclusion:** Ficheros que NO se tienen en cuenta para escanear

### Para ejecutar sonnar-scanner: 

1. Ir a la carpeta del proyecto para correr el escáner
`cd $PROJECT_NAME`

2. Correr
`docker run -ti -v $(pwd):/usr/src --link sonarqube --network sonarnet newtmitch/sonar-scanner`